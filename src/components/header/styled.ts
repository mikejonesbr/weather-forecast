import styled from "styled-components";

export const Container = styled.header`
    display: flex;
    justify-content: center;
    flex-direction: column;
`;

export const SearchInput = styled.input`
    margin-bottom: 15px;
    height: 18px;
    padding: 5px;
    font-size: 13px;
    background: #23222238;
    border: 1px solid #23222238;
    outline: none;
    color: #FFF;
    border-radius: 5px;
`;

export const SearchButton = styled.button`
    padding: 5px;
    font-size:15px;
    font-weight: bold;
    text-transform: uppercase;
    background: #b9b9b9ff;
    border: 1px solid #b9b9b9ff;
    cursor: pointer;
    border-radius: 5px;

    &:hover {
        background: #e2e2e2;
    }
`;

export const SearchLabel = styled.label<{
    htmlFor: string;
}>`
    font-weight: bold;
    font-size: 15px;
`;

export const Title = styled.h1`
    text-align: center;

`;