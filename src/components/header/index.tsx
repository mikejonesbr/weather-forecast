import React, { useState } from 'react';
import { noop } from "lodash";
import { Props } from "./types"
import { Container, SearchInput, SearchButton, SearchLabel, Title } from "./styled";

const Header = (props: Props) => {
    const [city, setCity] = useState("");
    return(
        <Container>
            <Title>Weather Forecast</Title>
            <SearchLabel htmlFor="search-input">Search by city</SearchLabel>
            <SearchInput id="search-input" type="text" value={city} onChange={(e) => {
                setCity(e.target.value)
            }}/>
            <SearchButton onClick={() => props.onSearch(city)}>Search</SearchButton>
        </Container>
    )
}

Header.defaultProps = {
    onSearch: noop
  }

export default Header;